#include <iostream>

using namespace std;

class Animal
{
protected:
	string voice;

public:
	virtual void Voice()
	{
		cout << voice << endl;
	}

	virtual ~Animal() { cout << "Animal destuct" << endl; }
};


class Dog : public Animal
{	
public:
	void Voice() override
	{
		voice = "Woof!";
		cout << voice << endl;
	}

	~Dog() { cout << "Dog destruct" << endl; }
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		voice = "Meaw!";
		cout << voice << endl;
	}

	~Cat() { cout << "Cat destruct" << endl; }
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		voice = "Moooo!";
		cout << voice << endl;
	}

	~Cow() { cout << "Cow destruct" << endl; }
};

class Snake : public Animal
{
public:
	void Voice() override
	{
		voice = "SSSSK!";
		cout << voice << endl;
	}

	~Snake() { cout << "Snake destruct" << endl; }
};

int main()
{
	
	const int size = 4;
	Animal *arr[size]{new Dog, new Cat, new Cow, new Snake};
	
	for (int i = 0; i < size; i++)
	{
		arr[i]->Voice();
	}

	cout << "_______________________________________" << endl;

	for (int i = 0; i < size; i++)
	{
		delete arr[i];
	}

	return 0;
}